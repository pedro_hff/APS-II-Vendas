import java.util.ArrayList;

public class CatalogoVendas {
	private ArrayList<Venda> vendas = new ArrayList<>();
	
	public int iniciarVenda(){
		Venda v = new Venda();
		vendas.add(v);
		return v.getId();
	}
	
	public void adicionaItem(int vendaID, int produtoID, CatalogoProdutos cat, int qtd){
		int index = buscaPorID(vendaID);
		Venda v = vendas.get(index);
		Produto p = cat.buscaPorId(produtoID);
		v.addItem(p, qtd);
		vendas.set(index, v);
	}
	
	//Tratar erro
	public int buscaPorID(int id){
		Venda v = new Venda();
		for (Venda venda : vendas) {
			if(venda.getId() == id)
				v = venda;
		}
		return vendas.indexOf(v);
	}
	
	public boolean finalizaVenda(int id){
		int index = buscaPorID(id);
		Venda v = this.vendas.get(index);
		if(v.isFinalizada()){
			System.out.println("Venda já finalizada");
			return false;
		}else{
			v.setFinalizada(true);
			v.printVenda();
			vendas.set(index, v);
			return true;
		}
		
	}
}
