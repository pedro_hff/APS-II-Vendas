import java.util.ArrayList;
import java.util.Date;

public class Venda {
	private static int newID = 0;
	private int id;
	private ArrayList<ItemVenda> itens = new ArrayList<>();
	private Date data;
	private boolean finalizada;
	
	public Venda() {
		this.id = ++newID;
		this.data = new Date();
		this.finalizada = false;
	}
	
	public void addItem(Produto prod, int qtde){
		ItemVenda iv = new ItemVenda(prod, qtde);
		itens.add(iv);
	}
	
	public double total(){
		float c = 0;
		for (ItemVenda itemVenda : itens) {
			c+=itemVenda.subtotal();
		}
		return c;
	}
	
	public void printVenda(){
		System.out.println("Venda ID: " + this.id);
		System.out.println("---");
		System.out.println("Itens(nome do produto - quantidade):");
		for (ItemVenda itemVenda : itens) {
			System.out.println("\t" + itemVenda);
		}
		System.out.println("Total: R$" + total());
	}
	
	//Getters & Setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<ItemVenda> getItens() {
		return itens;
	}

	public void setItens(ArrayList<ItemVenda> itens) {
		this.itens = itens;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public boolean isFinalizada() {
		return finalizada;
	}

	public void setFinalizada(boolean finalizada) {
		this.finalizada = finalizada;
	}
	
	
}
