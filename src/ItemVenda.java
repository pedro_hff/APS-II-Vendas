
public class ItemVenda {
	private Produto produto;
	private int quantidade;
	
	public ItemVenda(Produto p, int q) {
		this.produto = p;
		this.quantidade = q;
	}
	
	//Getters & Setters	
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public double subtotal(){
		return produto.getPreco() * this.quantidade;
	}
	
	@Override
	public String toString() {
		return this.produto.getNome() + " - " + this.quantidade;
	}
}
