import java.util.Scanner;

public class TPVD {
	public static CatalogoVendas cVendas = new CatalogoVendas();
	public static CatalogoProdutos cProdutos = new CatalogoProdutos();
	
	public void showMenu(Scanner scan){
		System.out.println("Digite uma opcao: ");
		System.out.println("1. Iniciar uma venda");
		System.out.println("2. Finalizar uma venda");
		System.out.println("3. Acrescentar produto à venda corrente");
		int opt = 4;
		
		opt = scan.nextInt();
		scan.nextLine();
		
		switch(opt){
			case 1:
				iniciaVenda(scan);
				break;
			case 2:
				finalizaVenda(scan);
				break;
			case 3:
				editarVenda(scan);
				break;
			default:
				System.out.println("Digite apenas os números 1,2 ou 3");
				break;
		}
//		try{
//		}catch(){
//			System.out.println("Digite apenas os números 1,2 ou 3++");
//			scan.close();
//			showMenu();
//		}
		
	}
	
	public void iniciaVenda(Scanner scan){
		int id = cVendas.iniciarVenda();
		System.out.println("Venda iniciada. ID " + id);
		System.out.println("Adicionar produtos à essa venda? (s/n)");
		try{
			String opt;
			opt = scan.nextLine();
			switch(opt){
				case "s":
					adicionaItemAVenda(id, scan);
					break;
				case "n":
					showMenu(scan);
					break;
				default:
					System.out.println("Digite apenas s ou n");
					break;
			}
		}catch(Exception e){
			System.out.println("Erro - Voce pode continuar adicionando produtos na venda de ID " + id);
//			scan1.close();
			showMenu(scan);
		}
	}
	
	public void adicionaItemAVenda(int id, Scanner scan){
		cProdutos.printProdutos();
		System.out.println("\nDigite o ID do produto a adicionar:");
		int produtoID = scan.nextInt();
		System.out.println("Quantos produtos serao adicionados?");
		int qtd = scan.nextInt();
		try{
			cVendas.adicionaItem(id, produtoID, cProdutos, qtd);			
		}catch (Exception e) {
			System.out.println(produtoID);
//			throw new Error(e);
		}
		
		System.out.println("Adicionar mais itens a essa venda?(s/n)");
		String opt = scan.next();
		if(opt.equals("s")){
			adicionaItemAVenda(id, scan);
		}else{
			showMenu(scan);
		}
	}
	
	public void finalizaVenda(Scanner scan){
		System.out.println("Digite o ID da venda para finalizar");
		int idVenda = scan.nextInt();
		try{
			cVendas.finalizaVenda(idVenda);
		}catch(Exception e){
			throw new Error(e);
		}
		
		System.out.println("---\n\n");
		showMenu(scan);
	}
	
	public void editarVenda(Scanner scan){
		System.out.println("Digite o ID da venda para adicionar itens");
		int idVenda = scan.nextInt();
		adicionaItemAVenda(idVenda, scan);
	}
	
	
}
