public class Produto {
	private static int newID = 0;
	private int id;
	private String descricao;
	private double preco;
	private String nome;
	
	public Produto(){}
	
	public Produto(String nome, String descricao, double preco) {
		newID++;
		this.id = newID;
		this.nome = nome;
		this.descricao = descricao;
		this.preco = preco;
	}
	
	//Getters & Setters	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return this.id + " - " + this.nome + " - " + this.preco;
	}
		
	
}
