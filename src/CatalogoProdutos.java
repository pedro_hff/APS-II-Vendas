import java.util.ArrayList;

public class CatalogoProdutos {
	private ArrayList<Produto> produtos = new ArrayList<>();
	
	public void addProduto(String nome, String descricao, double preco){
		Produto p = new Produto(nome, descricao, preco);
		produtos.add(p);
	}
	
	//tratar erro
	public Produto buscaPorId(int id){
		Produto p = new Produto();
		for (Produto produto : produtos) {
			if(produto.getId() == id)
				p = produto;
		}
		return p;
	}
	
	public void printProdutos(){
		System.out.println("Lista de Produtos (id - nome - preco)");
		for (Produto produto : produtos) {
			System.out.println(produto);
		}
	}
	
	public ArrayList<Produto> listaProdutos(String nome){
		ArrayList<Produto> prodList = new ArrayList<>();
		for (Produto produto : produtos) {
			if(produto.getDescricao().equals(nome))
				prodList.add(produto);
		}
		
		return prodList;
	}
	
	public ArrayList<Produto> listaProdutos(double precoMin, double precoMax){
		ArrayList<Produto> prodList = new ArrayList<>();
		for (Produto produto : prodList) {
			if(precoMax >= produto.getPreco() && precoMin <= produto.getPreco())
				prodList.add(produto);
		}
		
		return prodList;
	}

	public ArrayList<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(ArrayList<Produto> produtos) {
		this.produtos = produtos;
	}
	
	
}
